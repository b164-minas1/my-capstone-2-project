const mongoose = require("mongoose");

const reviewSchema = new mongoose.Schema(
{
	name: { type: String, required: true },
	comment: { type: String, required: true },
	rating: { type: Number, required: true },
},
	{
		timestamps: true,
	}

);

/*const productsSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Product is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	price:{
		type: Number,
		required: [true, "Price is required"]
	},
	quantity:{
		type: Number,
		required: [true, "Quantity is required"]
	},
/*	sku: {
		type: String,
		required: [true, "SKU number is required"]
	},
	categoryId: {
		type: String,
		required: [true, "Category Id is required"]
	},
	
	isActive:{
		type: Boolean,
		default: true
	},
	createdOn:{
		type: Date,
		default: new Date()
	},
	buyers: [
		{
			userId:{
				type: String,
				required: [true, "UserId is required"]
			},
			registeredOn: {
				type: Date,
				default: new Date()
			}
		}

	]
})
*/

const productSchema = new mongoose.Schema(
  {
    name: { type: String, required: true, unique: true },
    //seller: { type: mongoose.Schema.Types.ObjectID, ref: 'User' },
    image: { type: String, required: true },
    //brand: { type: String, required: true },
    category: { type: Array },
    description: { type: String, required: true},
    size: { type: String},
    color: { type: String},
    price: { type: Number, required: true },
    countInStock: { type: Number, required: true },
    rating: { type: Number, required: true },
    numReviews: { type: Number, required: true },
    reviews: [reviewSchema],
  },
  {
    timestamps: true,
  }
);


module.exports = mongoose.model( "Products", productsSchema)