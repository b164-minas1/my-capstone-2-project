const Product = require("../models/Product");
const isAdmin = require("../models/User");

module.exports.addProduct = (reqBody) => {
	console.log(reqBody);

	let newProduct = new Product({
		name: reqBody.product.name,
		description: reqBody.product.description,
		price: reqBody.product.price,
		quantity: reqBody.product.quantity
	});

	return newProduct.save().then((product,error) => {
		if(error) {
			return false;
		} else {
			return true;
		}
	})
}

//Retrieving All products
module.exports.getAllProducts = () => {
	return Product.find({}).then( result => {
		return result;
	})
}

module.exports.getAllActive = () => {
	return Product.find({ isActive: true }).then(result => {
		return result;
	})
}
//retrieving single product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams).then(result => {
		return result;
	})
}

//Update product

module.exports.updateProduct = (productId, reqBody) => {
	//specify the properties of the document to be updated
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		quantity: reqBody.quantity
	};

	//findByIdAndUpdate(id, updatesToBeApplied)
	return Product.findByIdAndUpdate(productId, updatedProduct).then((product, error) => {
		//product not updated
		if(error) {
			return false;
		} else {
			//product updated successfully
			return true;
		}
	})
}


module.exports.archiveProduct = (reqParams) => {
	//object
	let updateActiveField = {
		isActive : false
	}
	return Product.findByIdAndUpdate(reqParams, updateActiveField).then((product, error) => {
		if(error) {
			return false;
		} else {
			return true;
		}
	})
}

/*module.exports.checkoutProduct = (productId, reqBody) => {
	//specify the properties of the document to be updated
	let checkoutProduct = {
		productId: reqBody.productId,
		quantity: reqBody.quantity
	};
	return Product.findByIdAndCheckout(productId, checkoutProduct).then((product, error) => {
		
		if(error) {
			return false;
		} else {
			
			return true;
		}
	})
}
*/
module.exports.checkout = async (data) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.madeAnOrder = true;

		user.orderedProducts.push({
			productId: data.productId,
			quantity: data.quantity
		});
		return user.save().then((user, err) => {
			if(err) {
				return false;
			} else {
				return true;
			}
		})
	});

	let isProductUpdated = await Product.findById(data.productId).then(product => {
		product.quantity -= data.quantity;

		product.customers.push({
			userId: data.userId,
			quantityOrdered: data.quantity
		});
		return product.save().then((product, err) => {
			if(err) {
				return false;
			} else {
				return true
			}
		})
	})
	if(isUserUpdated && isProductUpdated) {
		return {message: 'Product has been added'}
	} else {
		return false;
	}
}