const User = require("../models/User");
const Product = require("../models/Product");

const bcrypt = require('bcrypt');
const auth = require("../auth")




module.exports.checkEmailExists = (reqBody) => {
	return User.find({ email: reqBody.email }).then(result => {
		//if match is found
		if(result.length > 0){
			return true;
		} else {
			//No duplicate email found
			return false;
		}
	})
}

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		mobileNo: reqBody.mobileNo,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password,10 )
	})
	return newUser.save().then((user, error) => {
		if(error) {
			return false;
		} else {
			return true
		}
	})
}

module.exports.loginUser = (reqBody) => {
	//findOne it will return the first record in the collection that matches the search criteria
	return User.findOne({ email: reqBody.email}).then(result => {
		//User does not exist
		if(result == null){
			return false;
		} else {
			//User exists

			//The "compareSync" method is used to compare a non encrypted password form the login form to the encrypted password retrieved from the database and reutrns "true" or "false"
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			//if the password match
			if(isPasswordCorrect){
				//Generate an access token
				return { accessToken : auth.createAccessToken(result.toObject())}
			} else {
				//Password does not match
				return false;
			}
		}
	})
}

module.exports.getProfile = (reqBody) => {
	return User.findById(reqBody).then(result => {
		if(result == null){
			return false;
		}else {
			result.password = "";
			return result
		}
	})
}

module.exports.getAdmin = () => {
	return User.find({ isAdmin: true }).then(result => {
		return result;
	})
}
