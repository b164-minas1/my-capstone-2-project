const Cart = require("../models/Cart");
const User = require("../models/User");

module.exports.addItemToCart = async (req, res) => {
    const {
        userId,
        productId
    } = req.body;
    const quantity = Number.parseInt(req.body.quantity);

    try {
        // Get users Cart 
        let cart = await Cart.findOne({
            userId: userId
        })

        //Get Selected Product Details 
        const productDetails = await Product.findById(productId);

        //Check if cart Exists and Check the quantity of items 
        if (!cart && quantity item.productId == productId);

            

            //Check if Quantity is Greater than 0 then add item to items Array
            else if (quantity > 0) {
                cart.items.push({
                    productId: productId,
                    quantity: quantity,
                    price: productDetails.price,
                    total: parseInt(productDetails.price * quantity)
                })
                cart.subTotal = cart.items.map(item => item.total).reduce((acc, next) => acc + next);
            }
             
            else {
                return res.status(400).json({
                    type: "Invalid",
                    msg: "Invalid request"
                })
            }
            let data = await cart.save();
            res.status(200).json({
                type: "success",
                mgs: "Process Successful",
                data: data
            })
        }
        //if there is no user with a cart...it creates a new cart and then adds the item to the cart that has been created
        else {
            const cartData = {
                userId: userId,
                items: [{
                    productId: productId,
                    quantity: quantity,
                    total: parseInt(productDetails.price * quantity),
                    price: productDetails.price
                }],
                subTotal: parseInt(productDetails.price * quantity)
            }
            cart = new Cart(cartData);
            let data = await cart.save();
            res.json(data);
        }
    catch (err) {
        console.log(err)
        res.status(400).json({
            type: "Invalid",
            msg: "Something Went Wrong",
            err: err
        })
    }


