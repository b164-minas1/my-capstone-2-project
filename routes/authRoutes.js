const router = require("express").Router();
const User = require("../models/User");

//REGISTER
router.post("/register", async (req,res) => {
	const newUser = new User({
		username: req.body.username,
		email: req.body.email,
		password: req.body.password,
	});
	try {
	const savedUser = await newUser.save();
	res.status(201).json(savedUser)
   } catch (err) {
   	res.status(500).json(err);
   }
});

//login

router.post("login", async(req, res) => {
	try{

		const user = await User.findOne({ username: req.body.username})
		const hashedPassword = CryptoJS.AES.decrypt(
			user.password,
			process.env.PASS_SEC);
		const password = hashedPassword.toString(CryptoJS.enc.Utf8);
		password != req.body.password && res.status(401).json("Wrong credentials!");
		const { password, ...others } = user;
		res.status(200).json(others);
		}catch (err) {
			res.status(500).json(err);
		
	}
})

module.exports = router;