const express = require("express");
const router = express.Router();
const ProductController = require("../controllers/productControllers")

const auth = require('../auth')

router.post("/", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin) {
		ProductController.addProduct(data).then(result => res.send(result));
	} else {
		res.send({ auth: "You're not an admin"})
	}
})



//Retrieving ALL products
router.get("/all", (req, res) => {
	ProductController.getAllProducts().then(result => res.send(result));
})

//Retrieving all ACTIVE Products
router.get("/", (req, res) => {
	ProductController.getAllActive().then(result => res.send(result))
});

router.get("/:productId", (req, res) => {
	console.log(req.params.productId);

	ProductController.getProduct(req.params.productId).then(result => res.send(result))
});

//updating products

router.put("/:productId", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		ProductController.updateProduct(req.params.productId, req.body).then(result => res.send(result));
	}else {
		res.send(false)
	}
})

router.put('/:productId/archive', auth.verify, (req, res) =>{
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		ProductController.archiveProduct(req.params.productId).then(result => res. send(result))
	} else {
		res.send(false);
	}
})

router.post('/checkout', auth.verify, (req, res) => {

	let data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId,
		quantity: req.body.quantity
	}
	UserController.checkout(data).then(result => res.send(result));
});



module.exports = router;