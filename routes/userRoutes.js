const express = require("express");
const router = express.Router();
const auth = require("../auth");

const UserController = require("../controllers/userControllers")
router.post("/checkEmail", (req, res) => {
	UserController.checkEmailExists(req.body).then(result => res.send(result))
});

router.post("/register", (req, res) => {
	UserController.registerUser(req.body).then(result => res.send(result))
});
router.post("/login", (req, res) => {
	UserController.loginUser(req.body).then(result => res.send(result))
})

router.get("/details", auth.verify, (req, res) => {
	//decode() to retrieve the user information from the token passing the "token" from the  request headers as an argument

	const userData = auth.decode(req.headers.authorization);
	UserController.getProfile(userData.id).then(result => res.send(result))
} )
router.get("/admin", (req, res) => {
	console.log(req.params.userId);

	UserController.getAdmin(req.params.userId).then(result => res.send(result))
});












module.exports = router;